# -*- coding: utf-8 -*-

import os
import json
import time

from tornado import web, ioloop
from sockjs.tornado import SockJSRouter, SockJSConnection

from django.core.wsgi import get_wsgi_application
os.environ['DJANGO_SETTINGS_MODULE'] = 'zenlog.settings'
application = get_wsgi_application()

from zenlog.models import Log


class LogConnection(SockJSConnection):
    def on_open(self, info):
        self.send(json.dumps({'connected': True}))

    def on_message(self, data):
        try:
            data = json.loads(data)
            project_name = str(data['project_name']) if 'project_name' in data else None
            try:
                latest_id = int(data['latest_id'])
            except:
                latest_id = None
            timeout = 30
            sleep_time = 0.3

            while True:
                if latest_id:
                    query = Log.objects.filter(id__gt=latest_id)
                else:
                    query = Log.objects
                if project_name:
                    query = query.filter(project__name__iexact=project_name)

                result = [x.serialize() for x in list(query.order_by('pk').all()[:100])]
                if result:
                    self.send(json.dumps({'logs': result}))
                    latest_id = result[-1]['id']  # Update next
                if timeout <= 0:
                    break
                timeout -= sleep_time
                time.sleep(sleep_time)
            data = {
                'success': True,
                'latest_id': latest_id,
            }
            self.send(json.dumps(data))
        except Exception as e:
            self.send(json.dumps({'error': 'Error %s' % e}))


if __name__ == '__main__':
    import logging

    port = 8080

    logging.getLogger().setLevel(logging.INFO)

    Router = SockJSRouter(LogConnection, '/ws/log')

    app = web.Application(Router.urls)
    app.listen(port)

    logging.info(" [*] Listening on 0.0.0.0:{}".format(port))

    ioloop.IOLoop.instance().start()
