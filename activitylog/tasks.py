# -*- coding: utf-8 -*-

from __future__ import absolute_import

from celery import shared_task, task

from django.db import transaction, OperationalError

from .models import ActivityProject, ActivityUser, ActivityLog


# @task
@shared_task
def save_activitylog(project_name, user_username, user_nickname, tags_name, message):
    def unique(l):
        ret = []
        for item in l:
            if item and item not in ret:
                ret.append(item)
        return ret
    try:
        tags = [x.strip() for x in tags_name.split(',') + [project_name, user_username] if x]
        unique_tags = unique(tags)
        with transaction.atomic():
            project, _     = ActivityProject.objects.get_or_create(name=project_name)
            user, _        = ActivityUser.objects.get_or_create(username=user_username, defaults={'nickname': user_nickname})
            entity = ActivityLog(message=message, project=project, user=user)
            entity.save()
            if tags_name:
                entity.tags.add(*unique_tags)
    except OperationalError as e:
        print e
    return
