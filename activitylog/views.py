# -*- coding: utf-8 -*-

import time

from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.db.models import Q

from utils.decorators import json_response
from .models import ActivityLog, ActivityFavoriteFilter
from .forms import ActivityLogForm


@login_required
def index(request):
    project_name  = request.GET.get('project_name', None)
    user_username = request.GET.get('user_username', None)
    q             = request.GET.get('q', None)
    limit         = int(request.GET.get('limit', '100'))
    query         = ActivityLog.objects
    if q:
        q_elem = [x.strip() for x in q.split(',')]  # Strip for whitespace
        query = query.filter(Q(tags__name__in=q_elem) | Q(project__name__in=q_elem))
    if project_name:
        query = query.filter(project__name__iexact=project_name)
    if user_username:
        query = query.filter(user__username__iexact=user_username)
    query = query.order_by('-created_on').all()[:limit][::-1]
    data = {
        'logs': query,
        'activity_log_filters': ActivityFavoriteFilter.objects.all(),
    }
    return render(request, 'activitylog/index.html', data)


@csrf_exempt
@json_response
def api(request):
    data = {
        'success': False,
    }
    form = ActivityLogForm(request.POST)
    if form.is_valid():
        form.save()
        data['success'] = True
    return data
