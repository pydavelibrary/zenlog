from django import forms
from .models import ActivityUser, ActivityProject, ActivityLog
from django.db import transaction
# from .tasks import save_activitylog


class ActivityLogForm(forms.ModelForm):

    class Meta:
        model = ActivityLog
        fields = ['message']

    user_username = forms.CharField(max_length=255)
    user_nickname = forms.CharField(max_length=255, required=False)
    project_name = forms.CharField(max_length=255)
    tags_name    = forms.CharField(max_length=511, required=False)

    def save(self, commit=True):
        # user_nickname = self.cleaned_data['user_nickname'] if self.cleaned_data['user_nickname'] else None
        # save_activitylog.delay(
        #     project_name=self.cleaned_data['project_name'],
        #     user_username=self.cleaned_data['user_username'],
        #     user_nickname=user_nickname,
        #     tags_name=self.cleaned_data['tags_name'],
        #     message=self.cleaned_data['message'],
        # )
        def unique(l):
            ret = []
            for item in l:
                if item and item not in ret:
                    ret.append(item)
            return ret
        project_name = self.cleaned_data['project_name']
        user_username = self.cleaned_data['user_username']
        tags_name = self.cleaned_data['tags_name']  # default = ''
        tags = [x.strip() for x in tags_name.split(',') + [project_name, user_username] if x]
        unique_tags = unique(tags)
        with transaction.atomic():
            project, _     = ActivityProject.objects.get_or_create(name=project_name)
            nickname       = self.cleaned_data['user_nickname'] if self.cleaned_data['user_nickname'] else None
            user, _        = ActivityUser.objects.get_or_create(username=user_username, defaults={'nickname': nickname})
            entity         = super(ActivityLogForm, self).save(commit=False)
            entity.project = project
            entity.user    = user
            if commit:
                entity.save()
                if tags_name:
                    entity.tags.add(*unique_tags)
        return entity
