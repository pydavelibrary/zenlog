# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('activitylog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitylog',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 17, 16, 46, 35, 71000), auto_now_add=True, db_index=True),
            preserve_default=False,
        ),
    ]
