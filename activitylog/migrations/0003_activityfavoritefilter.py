# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('activitylog', '0002_activitylog_created_on'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityFavoriteFilter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('project_name', models.CharField(max_length=50, null=True, blank=True)),
                ('user_username', models.CharField(max_length=50, null=True, blank=True)),
                ('q', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'ActivityFavoriteFilter',
                'verbose_name_plural': 'ActivityFavoriteFilters',
            },
        ),
    ]
