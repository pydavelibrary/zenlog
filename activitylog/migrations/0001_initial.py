# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField()),
            ],
            options={
                'verbose_name': 'ActivityLog',
                'verbose_name_plural': 'ActivityLogs',
            },
        ),
        migrations.CreateModel(
            name='ActivityProject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('url', models.URLField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'ActivityProject',
                'verbose_name_plural': 'ActivityProjects',
            },
        ),
        migrations.CreateModel(
            name='ActivityUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=255)),
                ('nickname', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'ActivityUser',
                'verbose_name_plural': 'ActivityUsers',
            },
        ),
        migrations.AddField(
            model_name='activitylog',
            name='project',
            field=models.ForeignKey(to='activitylog.ActivityProject'),
        ),
        migrations.AddField(
            model_name='activitylog',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', help_text='A comma-separated list of tags.', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='activitylog',
            name='user',
            field=models.ForeignKey(to='activitylog.ActivityUser'),
        ),
    ]
