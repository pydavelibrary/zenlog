from django.contrib import admin
from .models import ActivityUser, ActivityProject, ActivityLog, ActivityFavoriteFilter


admin.site.register(ActivityUser)
admin.site.register(ActivityProject)
admin.site.register(ActivityLog)
admin.site.register(ActivityFavoriteFilter)
