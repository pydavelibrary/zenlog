from django.views.generic.base import RedirectView
from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/$', views.api, name='api'),
]
