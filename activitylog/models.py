# -*- coding: utf-8 -*-

from __future__ import unicode_literals

try:
    from urllib.parse import urlencode  # Python 3
except:
    from urllib import urlencode


from django.utils.encoding import python_2_unicode_compatible
from django.core.urlresolvers import reverse
from django.db import models
from taggit.managers import TaggableManager


@python_2_unicode_compatible
class ActivityUser(models.Model):

    class Meta:
        verbose_name = "ActivityUser"
        verbose_name_plural = "ActivityUsers"

    username = models.CharField(max_length=255)
    nickname = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        if self.nickname:
            return self.nickname
        return self.username


@python_2_unicode_compatible
class ActivityProject(models.Model):

    class Meta:
        verbose_name = "ActivityProject"
        verbose_name_plural = "ActivityProjects"

    def __str__(self):
        return self.name

    name = models.CharField(max_length=255)
    url  = models.URLField(blank=True, null=True)


@python_2_unicode_compatible
class ActivityLog(models.Model):

    class Meta:
        verbose_name = "ActivityLog"
        verbose_name_plural = "ActivityLogs"

    def __str__(self):
        return self.message

    user       = models.ForeignKey(ActivityUser)
    project    = models.ForeignKey(ActivityProject)
    message    = models.TextField()
    tags       = TaggableManager()
    created_on = models.DateTimeField(db_index=True, auto_now_add=True)


@python_2_unicode_compatible
class ActivityFavoriteFilter(models.Model):

    class Meta:
        verbose_name = "ActivityFavoriteFilter"
        verbose_name_plural = "ActivityFavoriteFilters"

    def __str__(self):
        return self.name

    name          = models.CharField(max_length=50)
    project_name  = models.CharField(max_length=50, null=True, blank=True)
    user_username = models.CharField(max_length=50, null=True, blank=True)
    q             = models.CharField(max_length=255, null=True, blank=True)

    @property
    def url(self):
        params = {}
        if self.project_name:
            params['project_name'] = self.project_name
        if self.user_username:
            params['user_username'] = self.user_username
        if self.q:
            params['q'] = self.q
        return reverse('activitylog:index') + '?' + urlencode(params)
