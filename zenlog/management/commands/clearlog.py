#!/usr/bin/python
# -*- coding: utf-8

import subprocess
import datetime
import time
import os

from django.core.management.base import BaseCommand
from django.conf import settings

from smartlog.smartlog import SmartLog
from zenlog.models import SiteConfiguration, Log


class Command(BaseCommand):

    def handle(self, *args, **options):

        logger = SmartLog.getLogger('zenlog').add_tag('clearlog')
        config = SiteConfiguration.get_solo()

        # while True:
        config.reload()
        if config.auto_clearlog and config.auto_clearlog_days > 0:
            logger.debug('clearing log older than %s days' % (config.auto_clearlog_days))
            clear_until_date = datetime.date.today() - datetime.timedelta(days=config.auto_clearlog_days)
            overall_count    = 0
            for delete_count, all_count in Log.clear_log(created_on__lt=clear_until_date):
                overall_count = delete_count
                logger.debug('%d logs cleared of %d' % (delete_count, all_count))
            if os.name == 'posix':
                # find ./media -mtime +7 -delete
                # find /srv/www/logs.com/zenlog/media -mtime +30 -delete
                params = ["find", settings.MEDIA_ROOT, '-mtime', '+%d' % config.auto_clearlog_days, '-delete']
                logger.debug('clearing log attachment [%s].' % ' '.join(params))
                subprocess.call(params)
            logger.info('total %d logs cleared.' % (overall_count))
        # time.sleep(60)
