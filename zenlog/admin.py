from django.contrib import admin
from .models import Project, Log, SiteConfiguration
from solo.admin import SingletonModelAdmin


admin.site.register(Project)
admin.site.register(Log)
admin.site.register(SiteConfiguration, SingletonModelAdmin)
