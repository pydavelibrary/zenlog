# -*- coding: utf-8 -*-

import datetime

from django import forms
from django.utils import timezone
from .models import Project, Log
from django.db import transaction


class LogForm(forms.ModelForm):

    class Meta:
        model = Log
        fields = ['group_uid', 'message', 'attachment', 'level']

    project_name = forms.CharField(max_length=255)
    created_on   = forms.DateTimeField(required=False)
    tags_name    = forms.CharField(max_length=511, required=False)

    def save(self, commit=True):

        def unique(l):
            ret = []
            for item in l:
                if item and item not in ret:
                    ret.append(item)
            return ret
    
        entity = super(LogForm, self).save(commit=False)
        project, _ = Project.objects.get_or_create(name=self.cleaned_data['project_name'])
        entity.project = project

        if commit:
            entity.created_on = self.cleaned_data['created_on'] or timezone.now()
            entity.save()
            tags_name = self.cleaned_data['tags_name']  # default = ''
            tags = [x.strip() for x in tags_name.split(',') + [project.name] if x.strip()]  # .lower()
            if tags_name:
                tags = unique(tags)
                entity.tags.add(*tags)
                # for tag in tags:
                #     entity.tags.add(tag)
                # with transaction.atomic():
                # try:
                # entity.tags.add(*unique(tags))
                # except IntegrityError:
                #     pass
        return entity


def try_parse_datetime(formatter, d):
    try:
        d = datetime.datetime.strptime(d, formatter)
    except:
        return False
    return d


class SearchLogForm(forms.Form):

    start_date = forms.CharField(required=False)
    stop_date  = forms.CharField(required=False)
    q          = forms.CharField(required=False)
    q_project  = forms.CharField(required=False)
    group_uid  = forms.CharField(required=False)
    limit      = forms.IntegerField(required=False)

    def parse_datetime(self, s):
        if s:
            now = datetime.datetime.now()
            result = try_parse_datetime('%Y-%m-%d %H:%M:%S', s)
            if not result:
                result = try_parse_datetime('%b %d %H:%M:%S', s)
                if result:
                    result = result.replace(year=now.year)
            if not result:
                result = try_parse_datetime('%H:%M:%S', s)
                if result:
                    result = result.replace(year=now.year, month=now.month, day=now.day)
            if not result:
                raise forms.ValidationError('Invalid datetime format.')
            return result

    def clean_start_date(self):
        start_date = self.cleaned_data['start_date']
        return self.parse_datetime(start_date)

    def clean_stop_date(self):
        stop_date = self.cleaned_data['stop_date']
        return self.parse_datetime(stop_date)
