# -*- coding: utf-8 -*-

import zipfile
import gzip
import time
import os

from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.db.models import Q

from utils.decorators import json_response
from .models import Log, Project
from .forms import LogForm, SearchLogForm


@login_required
def index(request):
    form           = SearchLogForm(request.GET)
    default_limit  = 100
    limit          = default_limit
    q              = ''
    q_project      = ''
    group_uid      = ''
    start_date     = None
    stop_date      = None
    start_date_str = ''
    stop_date_str  = ''
    limit_revert   = True

    if form.is_valid():
        limit          = form.cleaned_data['limit']
        q              = form.cleaned_data['q']
        q_project      = form.cleaned_data['q_project']
        group_uid      = form.cleaned_data['group_uid']
        start_date     = form.cleaned_data['start_date']
        start_date_str = request.GET.get('start_date', '')
        stop_date      = form.cleaned_data['stop_date']
        stop_date_str  = request.GET.get('stop_date', '')
        if not limit and (not start_date or not stop_date):  # if unlimit but no boundary, limit it!
            limit = default_limit

    query = Log.objects

    if q:
        q_elem = [x.strip() for x in q.split(',')]  # Strip for whitespace
        project_ids = Project.objects.filter(name__in=q_elem).values_list('pk', flat=True)
        if project_ids:
            query = query.filter(project__id=project_ids[0])
        query = query.filter(tags__name__in=q_elem)
        # query = query.filter(Q(tags__name__in=q_elem) | Q(project__name__in=q_elem))

    if q_project:
        project_query = Project.objects.filter(name__iexact=q_project).first()
        if project_query:
            query = query.filter(project__id=project_query.pk)
        # query = query.filter(project__name__iexact=q_project)
    if group_uid:
        query = query.filter(group_uid__iexact=group_uid)

    if start_date:
        query = query.filter(created_on__gte=start_date)
        if stop_date:
            query = query.filter(created_on__lte=stop_date)
        else:
            limit_revert = False

    if limit:
        if limit_revert:
            query = query.order_by('-created_on', '-pk').all().distinct()[:limit][::-1]
        else:
            query = query.order_by('created_on', 'pk').all().distinct()[:limit]  # order_by('pk')
    else:
        query = list(query.all().distinct())

    is_realtime = not (start_date or stop_date or group_uid)
    data = {
        'logs': query,
        'q': q,
        'q_project': q_project,
        'start_date': start_date_str,
        'stop_date': stop_date_str,
        'limit': limit,
        'is_realtime': is_realtime,
    }
    return render(request, 'index.html', data)


@login_required
def preview(request, pk):
    entity = get_object_or_404(Log, pk=pk)
    if not entity.attachment:
        raise Http404()
    if not os.path.isfile(entity.attachment.path):
        raise Http404()

    attachment_path = entity.attachment.path
    if attachment_path.endswith('.zip'):
        z = zipfile.ZipFile(open(attachment_path, 'rb'))
        try:
            source = z.read('attachment.txt')
        except:
            source = ''
    elif attachment_path.endswith('.gz'):
        with gzip.open(attachment_path, 'rb') as z:
            source = z.read()
    else:
        source = open(attachment_path, 'rb').read()
    data = {
        'log': entity,
        'source': source,
    }
    return render(request, 'preview.html', data)


@login_required
def attachment(request, pk):
    entity = get_object_or_404(Log, pk=pk)
    if not entity.attachment:
        raise Http404()
    if not os.path.isfile(entity.attachment.path):
        raise Http404()

    attachment_path = entity.attachment.path
    if attachment_path.endswith('.zip'):
        z = zipfile.ZipFile(open(attachment_path, 'rb'))
        try:
            source = z.read('attachment.txt')
        except:
            source = ''
    elif attachment_path.endswith('.gz'):
        with gzip.open(attachment_path, 'rb') as z:
            source = z.read()
    else:
        source = open(attachment_path, 'rb').read()
    data = {
        'log': entity,
        'source': source,
    }
    return render(request, 'source.html', data)


@json_response
def ajax_log(request):
    if not request.user.is_authenticated():
        return {
            'success': False,
            'type': 'login_required',
        }
    try:
        latest_id = int(request.GET.get('latest_id'))
    except:
        latest_id = None
    query = Log.objects
    if latest_id:
        query = query.filter(id__gt=latest_id)
    limit = 30
    while True:
        result = [x.serialize() for x in list(query.all()[:100])]
        if result or limit <= 0:
            break
        limit -= 1
        time.sleep(1)
    data = {
        'success': True,
        'logs': result,
    }
    return data


@json_response
def api_readlog(request):
    uid = request.GET.get('uid', None)
    if not uid:
        return {
            'success': False,
        }
    messages = Log.objects.filter(group_uid__iexact=uid).all()
    messages = [x.serialize() for x in list(messages[:200])]
    return {
        'success': True,
        'logs': messages,
    }


@csrf_exempt
@json_response
def api(request):
    data = {
        'success': False,
    }
    form = LogForm(request.POST, request.FILES)
    if form.is_valid():
        form.save()
        data['success'] = True
    return data


@json_response
def api_disk(request):
    from utils.disk import Disk
    return {
        'success': True,
        'disks_usage': list(Disk().disk_usage()),
    }
