from django.contrib.auth.decorators import login_required
from django.views.generic.base import RedirectView
from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from django.views.static import serve
from . import views
if getattr(settings, 'SSO_URL', False):
    admin.site.login = login_required(admin.site.login)


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^attachment/(?P<pk>\d+)/$', views.attachment, name='attachment'),
    url(r'^attachment/(?P<pk>\d+)/preview/$', views.preview, name='preview'),
    url(r'^ajax/log/$', views.ajax_log, name='ajax_log'),
    url(r'^api/$', views.api, name='api'),
    url(r'^api/readlog/$', views.api_readlog, name='api_readlog'),
    url(r'^api/disk/$', views.api_disk, name='api_disk'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^activitylog/', include('activitylog.urls', namespace='activitylog')),

    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico', permanent=False))
]

urlpatterns += [
    url(r'^media/(?P<path>.*)$', serve,
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True})
]
