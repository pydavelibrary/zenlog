# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import datetime
import time

from django.utils.encoding import python_2_unicode_compatible
from django.db import models
import random

from taggit.managers import TaggableManager
from solo.models import SingletonModel


@python_2_unicode_compatible
class Project(models.Model):

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"

    name = models.CharField(max_length=255, unique=True, db_index=True)

    def __str__(self):
        return self.name


def log_upload_to(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    today = datetime.date.today()
    date = today.strftime('%Y/%m/%d')
    random_name = random.choice('abcdefghijklmnopqrstuvwxyz0123456789')
    upload_path = 'attachments/%s/%s/%s' % (date, random_name, filename)
    return upload_path


@python_2_unicode_compatible
class Log(models.Model):

    class Meta:
        verbose_name = "Log"
        verbose_name_plural = "Logs"

    project    = models.ForeignKey(Project)
    group_uid  = models.CharField(db_index=True, max_length=32)
    tags       = TaggableManager()
    message    = models.TextField(blank=True)
    attachment = models.FileField(blank=True, null=True, upload_to=log_upload_to)  # 'attachments/%Y/%m/%d')
    created_on = models.DateTimeField(db_index=True)
    level      = models.CharField(max_length=32)

    def __str__(self):
        return self.message

    def serialize(self):
        return {
            'id': self.pk,
            'project_name': self.project.name,
            'tags': [str(x) for x in self.tags.all()],
            'message': self.message,
            'created_on': self.created_on.strftime('%b %d %H:%M:%S'),
            'attachment': self.attachment.url if self.attachment else '',
            'level': self.level,
        }

    @staticmethod
    def clear_log(**_filter):
        # q       = Log.objects.filter(**_filter).values_list('id', flat=True)
        # cnt     = q.count()
        q = Log.objects.first()
        cnt = -1
        bucket  = 10000

        done    = 0
        datetime_filter = datetime.datetime.combine(_filter['created_on__lt'], datetime.time(0, 0))
        while True:
            # objs    = list(q[:bucket])
            # count = len(objs)
            # if not objs:
            #     break
            # Log.objects.filter(pk__gte=objs[0], pk__lte=objs[-1]).delete()
            # done += count

            '''
            Re-Implement for large data.
            '''
            q = Log.objects.filter(pk=q.pk + bucket).first()
            if not q:
                break
            if q.created_on >= datetime_filter:
                break
            Log.objects.filter(pk__lte=q.pk).delete()
            done += bucket

            yield done, cnt
            time.sleep(0.01)


@python_2_unicode_compatible
class SiteConfiguration(SingletonModel):

    class Meta:
        verbose_name = 'Site Configuration'

    auto_clearlog      = models.BooleanField(default=True)
    auto_clearlog_days = models.PositiveIntegerField(default=30)

    def __str__(self):
        return 'Site Configuration'

    def reload(self):
        new_self = self.__class__.objects.get(pk=self.pk)
        self.__dict__.update(new_self.__dict__)
