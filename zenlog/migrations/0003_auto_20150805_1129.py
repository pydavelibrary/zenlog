# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zenlog', '0002_auto_20150720_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='attachment',
            field=models.FileField(null=True, upload_to='attachments/%Y/%m/%d', blank=True),
        ),
    ]
