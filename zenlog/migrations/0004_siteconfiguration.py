# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zenlog', '0003_auto_20150805_1129'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auto_clearlog', models.BooleanField(default=True)),
                ('auto_clearlog_days', models.PositiveIntegerField(default=30)),
            ],
            options={
                'verbose_name': 'Site Configuration',
            },
        ),
    ]
