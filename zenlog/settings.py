from .common_settings import *


INSTALLED_APPS += [
    'taggit',
    'zenlog',
    'activitylog',
    # 'djcelery',
    # 'kombu.transport.django',
]
