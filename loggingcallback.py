# -*- coding: utf-8 -*-


def loggingcallback(record):
    record.levelname = 'WARNING'
    record.levelno = 30  # WARNING
    return record
